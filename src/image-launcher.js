import ImageResizer from './image-resizer'

class ImageLauncher {
    constructor(selector, options = {}) {
        this.originalSelector = selector
        this.selector = document.querySelectorAll(selector)
        this.options = options
        this.instances = []
        this.init()
    }

    init() {
        this.selector.forEach((element) => {
            this.instances.push(new ImageResizer(element, this.options))
        })
    }

    getUniques() {
        const selectors = document.querySelectorAll(this.originalSelector)
        selectors.forEach((element) => {
            if (element.querySelector('img').getAttribute('rendered')) return
            this.instances.push(new ImageResizer(element, this.options))
        })
    }

    redraw() {
        this.getUniques()
        this.instances.forEach((instance) => {
            instance.redrawInstance()
        })
    }
}
window.ImageLauncher = ImageLauncher
export default ImageLauncher
