class ImageResizer {
    constructor(element, options) {
        this.element = element
        this.imageElement = this.element.querySelector('img')
        this.loaded = false
        const defaults = {
            aspectRatio: 'auto',
            image: {
                element: null,
                srcset: '',
                offsetX: 0,
                offsetY: 0,
                newWidth: 0,
                newHeight: 0,
                aspectRatio: 0,
            },
            container: {
                width: 0,
                height: 0,
                aspectRatio: 0,
            },
            attribute: null,
        }

        this.settings = { ...defaults, ...options }
        this.bindWindowResize()
        this.init()
    }

    init() {
        if (this.settings.aspectRatio !== 'auto') this.settings.aspectRatio = parseFloat(this.settings.aspectRatio)
        this.attribute = (this.imageElement.hasAttribute('srcset') || this.imageElement.hasAttribute('data-srcset')) ? 'srcset' : 'src'
        if (this.imageElement.getAttribute(this.attribute) === 'null' || this.imageElement.getAttribute(this.attribute) === null) return
        this.initContainer().initImage()
    }

    initContainer() {
        return this.updateContainerSizes()
            .updateContainerAspectRatio()
            .setContainerHeight()
    }

    // ------------------------
    // INIT IMAGE
    // ------------------------
    initImage() {
        this.settings.image.element = this.element.querySelector('img')
        // eslint-disable-next-line
        this.settings.image[this.attribute] = this.settings.image.element.attributes[this.attribute].value
        this.settings.image.element.onload = () => {
            this.loaded = true
            this.setImageAspectRatio().calcImageOffset().drawImage()
        }

        if (!this.loaded && this.settings.image.element.complete) {
            this.loaded = true
            this.setImageAspectRatio().calcImageOffset().drawImage()
        }

        return this
    }

    updateContainerAspectRatio() {
        this.settings.container.aspectRatio = (this.settings.aspectRatio === 'auto') ? (this.settings.container.width / this.settings.container.height) : this.settings.aspectRatio
        return this
    }

    // ------------------------
    // UPDATE (GET) CONTAINER SIZES
    // ------------------------
    updateContainerSizes() {
        this.settings.container.width = this.element.offsetWidth
        this.settings.container.height = this.element.offsetHeight
        return this
    }

    // ------------------------
    // SET CONTAINER HEIGHT
    // ------------------------
    setContainerHeight() {
        // If settings are set to auto, leave the current Height
        if (this.settings.aspectRatio === 'auto') return this
        this.element.parentElement.classList.add('--setted-height')

        // Assume that settings are NOT set to auto, set a new height
        const newHeight = this.settings.container.width / this.settings.container.aspectRatio
        this.settings.container.height = newHeight
        this.element.style.height = `${newHeight}px`
        return this
    }

    // ------------------------
    // SET IMAGE ASPECT RATIO
    // ------------------------
    setImageAspectRatio() {
        // eslint-disable-next-line
        this.settings.image.aspectRatio = this.settings.image.element.width / this.settings.image.element.height
        return this
    }

    calcImageOffset() {
        let resizeFactor = null
        // If the image is wider than container
        if (this.settings.image.aspectRatio >= this.settings.container.aspectRatio) {
            resizeFactor = this.settings.container.height / this.settings.image.element.height
            this.settings.image.newWidth = this.settings.image.element.width * resizeFactor
            this.settings.image.newHeight = this.settings.image.element.height * resizeFactor
            // eslint-disable-next-line
            this.settings.image.offsetX = -((this.settings.image.newWidth - this.settings.container.width) / 2)
            this.settings.image.offsetY = 0
            return this
        }

        // Else, the image is higher than container
        resizeFactor = this.settings.container.width / this.settings.image.element.width
        this.settings.image.newWidth = this.settings.image.element.width * resizeFactor
        this.settings.image.newHeight = this.settings.image.element.height * resizeFactor
        this.settings.image.offsetX = 0
        // eslint-disable-next-line
        this.settings.image.offsetY = -((this.settings.image.newHeight - this.settings.container.height) / 2)

        return this
    }

    // ------------------------
    // DRAW IMAGE
    // ------------------------
    drawImage() {
        // Set the new width and height
        // eslint-disable-next-line
        this.settings.image.element.setAttribute('width', this.settings.image.newWidth)
        // eslint-disable-next-line
        this.settings.image.element.setAttribute('height', this.settings.image.newHeight)
        // Set the new offset
        this.settings.image.element.style.top = `${this.settings.image.offsetY}px`
        this.settings.image.element.style.left = `${this.settings.image.offsetX}px`
        // Add a resized attribute
        this.settings.image.element.setAttribute('rendered', true)
        // Return itself
        return this
    }

    redraw() {
        if (this.imageElement.getAttribute(this.attribute) === 'null' || this.imageElement.getAttribute(this.attribute) === null) return
        // eslint-disable-next-line
        if (!this.loaded) {
            this.initContainer().initImage()
            return
        }

        this
            .updateContainerSizes()
            .updateContainerAspectRatio()
            .setContainerHeight()
            .calcImageOffset()
            .drawImage()
    }

    bindWindowResize() {
        window.addEventListener('resize', () => {
            this.redraw()
        })
    }

    redrawInstance() {
        this.redraw()
    }
}

export default ImageResizer
