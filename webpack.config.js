const webpack = require('webpack-boilerplate/webpack.config.js')

webpack.entry = {
    'image-launcher': 'js/src/image-launcher.js',
}

module.exports = webpack
